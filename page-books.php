<?php
/**
 * Template Name: Books Page
 */


get_header(); ?>

    <div class="wrap">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <?php
                /* Start the Loop */
                $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

                $args = [
                    'post_type' => 'book',
                    'posts_per_page' => 1,
                    'paged'          => $paged,
                ];
                $new = new WP_Query($args);
                while ($new->have_posts()) :
                    $new->the_post();
                     //get_template_part( 'template-parts/post/content-book', get_post_format() );
                     get_template_part( 'template-parts/post/page-books', get_post_format() );
                endwhile;

                wp_reset_query();

                next_posts_link(  'next', $new->max_num_pages );
                previous_posts_link('prev');

                $big = 999999999; // уникальное число

                echo paginate_links( array(
                    'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format'  => '?paged=%#%',
                    'current' => max( 1, get_query_var('paged') ),
                    'total'   => $new->max_num_pages
                ) );
                ?>

            </main><!-- #main -->


        </div><!-- #primary -->

    </div><!-- .wrap -->

<?php get_footer();
