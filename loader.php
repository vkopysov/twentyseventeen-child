<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 21.02.2017
 * Time: 15:58
 */

require __DIR__ . '/vendor/autoload.php';

$car = new \TwentySeventeenChild\CarPostType();

$bookArgs = include (__DIR__.'/src/book-post-type.php');
$book = new \TwentySeventeenChild\BookPostType($bookArgs);

$bookMetaBox = new \TwentySeventeenChild\BookMetaBox();

$taxonomies = include (__DIR__.'/src/taxonomies.php');
foreach ( $taxonomies as $key => $value) {
    $authorBookTaxonomy = new \TwentySeventeenChild\BookTaxonomies($key, $value);
}

$themeOptions = new \TwentySeventeenChild\ThemeOption();

$navNenu = new \TwentySeventeenChild\NavMenu();

$widgetFilter = new \TwentySeventeenChild\TaxonomyFilterWidget();