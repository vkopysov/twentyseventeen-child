<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 9:39
 */

namespace TwentySeventeenChild;


class BookPostType
{
    private $args;

    public function __construct($args)
    {
        $this->args = $args;
        add_action( 'init', [ $this, 'register_book_post_type' ] );
    }

    public function register_book_post_type()
    {
        register_post_type('book', $this->args);
    }


}