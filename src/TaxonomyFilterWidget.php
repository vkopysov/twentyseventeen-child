<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 23.02.2017
 * Time: 16:12
 */

namespace TwentySeventeenChild;




class TaxonomyFilterWidget extends \WP_Widget {

    public function __construct() {
        // actual widget processes

        parent::__construct(
            'taxonomy_filter_widget',  // Base ID
            'Фильтр по таксономии'   // Name
        );
        add_action( 'widgets_init', function() {

            register_widget( __CLASS__ );
           // register_widget( ' \TwentySeventeenChild\TaxonomyFilterWidget' );
            }
        );
    }

    public  function widget($args, $instance)
    {
       // var_dump($args);
        echo $args['before_widget'];

        $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);

        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

        // WIDGET CODE GOES HERE
        if (isset($instance['title'])) {
            echo  "<h1> {$instance['title']} </h1>";
        }
        echo "<h2>This is my new widget!</h2>";

        echo $args['after_widget'];
    }

    public function form( $instance ) {
        // outputs the options form in the admin
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = "";
        }
        $count = isset($instance['count']) ? (bool) $instance['count'] :false;

        $taxonomies = get_taxonomies();

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
        foreach ($taxonomies as $taxonomy) {
            ?>
            <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
            <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />
            <?php
        }
        ?>
        <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
        <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />
        <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
        <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />
        <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
        <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />
        <?php
    }

    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = [];



        $instance['title'] = ( !empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

        $instance['text'] = ( !empty( $new_instance['text'] ) ) ? $new_instance['text'] : '';

        $instance['count'] = !empty($new_instance['count']) ? 1 : 0;

        return $instance;
    }

}