<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 10:33
 */

namespace TwentySeventeenChild;


class BookMetaBox
{
    public function __construct()
    {
        add_action('add_meta_boxes', [$this, 'add']);
        add_action('save_post', [$this, 'save']);
    }

    public function add()
    {
        add_meta_box(
            'book_meta_box',                // Unique ID
            'Дополнительная информация',   // Box title
            [$this, 'html'],                // Content callback, must be of type callable
            'book'                          // Post type
        );

    }

    public function save($post_id)
    {
        $book_data = [];
        $keys = ['pages', 'isbn', 'date'];
        foreach ($keys as $key) {
            if (array_key_exists($key, $_POST)) {
                $book_data[$key] = sanitize_text_field($_POST[$key]);
            }
        }

        update_post_meta($post_id, 'book_data', $book_data);
    }

    public static function html($post)
    {
        $book_data = get_post_meta($post->ID, 'book_data', true);

        if (!$book_data) {
            $book_data = [
                'pages' => '',
                'isbn' => '',
                'date' => ''
            ];
        }

        ?>
        <div >
            <label for="pages"> Pages: </label>
            <input class="widefat" type="text"  name="pages" value="<?= $book_data['pages'] ?>">
        </div>
        <div >
            <label for="date"> Release Date: </label>
            <input  class="widefat" type="text" name="date" value="<?= $book_data['date'] ?>">
        </div>
        <div >
            <labelfor="isbn"> ISBN: </label>
            <input  class="widefat" type="text"  name="isbn" value="<?= $book_data['isbn'] ?>">
        </div>
        <?php
    }

}