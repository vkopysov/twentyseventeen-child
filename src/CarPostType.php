<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 21.02.2017
 * Time: 16:40
 */

namespace TwentySeventeenChild;


class CarPostType
{
    private $args;

    public function __construct()
    {
        $this->args = [];
        add_action( 'init', [ $this, 'register_car_post_type' ] );
    }

    public function register_car_post_type()
    {
        $labels = [
            'name'          => __('Cars'),
            'singular_name' => __('Car'),
        ];

        $this->args = [
            'labels'             => $labels,
            'description'        => __( 'Car type is a test Custom Post type.', 'twenty-seventeen-child' ),
            'public'             => true,
            'capability_type'    => 'post',
            'query_var'          => true,
            'publicly_queryable' => true,
            'menu_position'      => 5,
            'rewrite'            => ['slug' => 'cars'],
        ];

        register_post_type('car', $this->args);
    }
}
