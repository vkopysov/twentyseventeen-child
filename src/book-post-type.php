<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 12:50
 */

return [
      'labels'             => [
          'name'               => __( 'Книги', 'twenty-seventeen-child' ),
          'singular_name'      => __( 'Книга', 'twenty-seventeen-child' ),
          'menu_name'          => __( 'Книги', 'twenty-seventeen-child' ),
          'name_admin_bar'     => __( 'Книга', 'twenty-seventeen-child' ),
          'add_new'            => __( 'Добавить новую', 'twenty-seventeen-child' ),
          'add_new_item'       => __( 'Добавить новую книгу', 'twenty-seventeen-child' ),
          'new_item'           => __( 'Новая книга', 'twenty-seventeen-child' ),
          'edit_item'          => __( 'Редактировать книгу', 'twenty-seventeen-child' ),
          'view_item'          => __( 'Просмотр книги', 'twenty-seventeen-child' ),
          'all_items'          => __( 'Все книги', 'twenty-seventeen-child' ),
          'search_items'       => __( 'Поиск книг', 'twenty-seventeen-child' ),
          'not_found'          => __( 'Книг не найдено.', 'twenty-seventeen-child' ),
          'not_found_in_trash' => __( 'В корзине не найдено.', 'twenty-seventeen-child' )
      ],
    'description'        => __( 'Пользовательский тип данных для добавления книг', 'twenty-seventeen-child' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'book' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'supports'           => [ 'title', 'editor', 'author', 'thumbnail'],
    'taxonomies'         => [ 'authors']
];