<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 23.02.2017
 * Time: 15:03
 */

namespace TwentySeventeenChild;


class NavMenu
{
    public function __construct()
    {
        add_filter( 'wp_nav_menu_items', [$this, 'bind_terms'], 10, 2 );
    }

    public function bind_terms($items)
    {
        $termArgs = [
            'taxonomy'      => [ 'author_b', 'genre' ]
        ];

        $terms = get_terms($termArgs);

        foreach ($terms as $term) {
            $items .= "<li><a href=\"". get_term_link( $term, $taxonomy = '' ) ."\">$term->name</a></li>";
        }

        return $items;
    }
}