<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 12:47
 */

namespace TwentySeventeenChild;


class ThemeOption
{
    public function __construct()
    {
        add_action( 'admin_menu', [ $this, 'add_theme_menu_item' ] );
        add_action("admin_init", [ $this, 'display_theme_panel_fields' ]);
    }

    public function add_theme_menu_item()
    {
        add_menu_page("Theme Panel", "Theme Panel", "manage_options", "theme-panel", [$this, 'theme_settings_page'], null, 99);
    }

    public function theme_settings_page()
    {
        ?>
        <div class="wrap">
            <h1>Theme Panel</h1>
            <form method="post"  enctype=multipart/form-data action="options.php">
                <?php
                settings_fields("section");
                do_settings_sections("theme-options");
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    public function display_theme_panel_fields()
    {
        add_settings_section("section", "Настроечки", null, "theme-options");

        add_settings_field("logo_c", "Выберите Логотип:", [$this, "logo_display"], "theme-options", "section");

        add_settings_field("copyright", "Введите Копирайт:", [$this, "copyright_display"], "theme-options", "section");

        register_setting("section", "logo_c", [$this,  "handle_logo"]);

        register_setting("section", "copyright", [$this,  "handle_copyright"]);

    }


    public function logo_display()
    {
        $option = get_option('logo_c');
        ?>
        <img src="<?= $option ?>"/>
        <br>
        <input type="file" name="logo"/>

        <?php
    }

    public function copyright_display()
    {
        $option = get_option('copyright');
        ?>
        <input type="text" name="copyright" value="<?php echo ($option) ? esc_attr($option) : "© default copyright" ?> "/>
        <?php
    }

    public function handle_logo()
    {
        global $option;

        if(!empty($_FILES))
        {
            $urls = wp_handle_upload($_FILES["logo"], array("test_form" => FALSE));
            $temp = $urls["url"];
            return $temp;
        }

        return $option;
    }

    public function handle_copyright() {
        if(isset($_POST['copyright'])) {
            return sanitize_text_field($_POST['copyright']);
        }
    }

}