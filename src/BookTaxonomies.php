<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 11:59
 */

namespace TwentySeventeenChild;


class BookTaxonomies
{
    private $name;
    private $args;

    public function __construct($name, $args)
    {
        $this->name = $name;
        $this->args = $args;
        add_action('init',[ $this, 'register_book_taxonomy']);
    }
    public function register_book_taxonomy()
    {
        register_taxonomy($this->name, ['book'], $this->args);
    }

}