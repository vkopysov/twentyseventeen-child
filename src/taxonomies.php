<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 12:13
 */
return [
    'author_b' => [
                'hierarchical'      => false, // make it hierarchical (like categories)
                'labels'            => [
                    'name'              => _x('Авторы', 'taxonomy general name'),
                    'singular_name'     => _x('Автор', 'taxonomy singular name'),
                    'search_items'      => __('Поиск авторов'),
                    'all_items'         => __('Все автора'),
                    'edit_item'         => __('Редактировать автора'),
                    'update_item'       => __('Обновить автора'),
                    'add_new_item'      => __('Добавить нового автора'),
                    'new_item_name'     => __('Новое имя автора'),
                    'menu_name'         => __('Авторы'),
                ],
                'show_ui'           => true,
                'show_admin_column' => true,
                'query_var'         => true,
                'rewrite'           => ['slug' => 'author_b'],
        ],
    'genre' => [
        'hierarchical'      => false, // make it hierarchical (like categories)
        'labels'            => [
            'name'              => _x('Жанры', 'taxonomy general name'),
            'singular_name'     => _x('Жанр', 'taxonomy singular name'),
            'search_items'      => __('Поиск жанра'),
            'all_items'         => __('Все жанры'),
            'edit_item'         => __('Редактировать жанры'),
            'update_item'       => __('Обновить жанр'),
            'add_new_item'      => __('Добавить новый жанр'),
            'new_item_name'     => __('Новое имя жанра'),
            'menu_name'         => __('Жанры'),
        ],
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => 'genre'],
    ]

    ];