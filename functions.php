<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 21.02.2017
 * Time: 15:24
 */


add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

require_once(__DIR__ . '/loader.php');


